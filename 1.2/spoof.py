from scapy.all import *

def sendIcmp(dst, src=''):
  a = IP()
  a.dst = dst
  a.src = src if src else a.src

  b = ICMP()
  
  p = a/b
  send(p)
 
sendIcmp('10.0.5.64', '10.0.5.64')
sendIcmp('10.0.4.80', '10.0.5.64')
sendIcmp('10.0.5.64', '10.0.4.80')
sendIcmp('10.0.5.64', '10.0.5.128')
sendIcmp('10.0.5.64', '10.6.4.128')
sendIcmp('10.10.10.10', '8.8.8.8')
