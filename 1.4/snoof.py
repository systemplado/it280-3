from scapy.all import *
import sys



def print_pkt(pkt):
  print('- - - - - - - - - - - - - - - - - -')
  pkt.show()

  ''' or check for arp instead?
  if ip.dst == ip.src
  and proto == icmp
  and icmp.type == dest-unreach
  and icmp.code == host-unreach
  '''
  if pkt.getlayer(IP).dst=='10.0.5.200':
    #spoof packet
    a = IP()
    a.dst = pkt.getlayer(IP).src
    a.src = pkt.getlayer(IP).dst

    req = pkt.getlayer(ICMP)
    checksum = req.getlayer(ICMP).chksum + 2048
    _id = req.id
    seq = req.seq-1

    b = ICMP(type='echo-reply', chksum=checksum, id=_id, seq=seq)
    c = Raw(load=pkt.getlayer(Raw).load)
    
    p = a/b/c
    send(p)



pkt = sniff(filter='', prn=print_pkt)
