from scapy.all import *
import sys;

def sendIcmp(dst, src='', ttl=64):
  a = IP()
  a.dst = dst
  a.src = src if src else a.src
  a.ttl = ttl

  b = ICMP()
  
  p = a/b
  send(p)
 
sendIcmp('8.8.8.8', ttl=int(sys.argv[1]))
