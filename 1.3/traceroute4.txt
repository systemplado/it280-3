- - - - - - - - - - - - - - - - - -
###[ Ethernet ]### 
  dst       = 88:51:fb:d1:02:00
  src       = 08:00:27:4a:51:92
  type      = 0x800
###[ IP ]### 
     version   = 4
     ihl       = 5
     tos       = 0x0
     len       = 28
     id        = 1
     flags     = 
     frag      = 0
     ttl       = 4
     proto     = icmp
     chksum    = 0x9791
     src       = 10.0.5.64
     dst       = 8.8.8.8
     \options   \
###[ ICMP ]### 
        type      = echo-request
        code      = 0
        chksum    = 0xf7ff
        id        = 0x0
        seq       = 0x0

- - - - - - - - - - - - - - - - - -
###[ Ethernet ]### 
  dst       = 08:00:27:4a:51:92
  src       = 88:51:fb:d1:02:00
  type      = 0x800
###[ IP ]### 
     version   = 4
     ihl       = 5
     tos       = 0x68
     len       = 56
     id        = 13995
     flags     = 
     frag      = 0
     ttl       = 252
     proto     = icmp
     chksum    = 0x2c2a
     src       = 202.90.129.237
     dst       = 10.0.5.64
     \options   \
###[ ICMP ]### 
        type      = time-exceeded
        code      = ttl-zero-during-transit
        chksum    = 0xf4ff
        reserved  = 0
        length    = 0
        unused    = None
###[ IP in ICMP ]### 
           version   = 4
           ihl       = 5
           tos       = 0x68
           len       = 28
           id        = 1
           flags     = 
           frag      = 0
           ttl       = 1
           proto     = icmp
           chksum    = 0x9a29
           src       = 10.0.5.64
           dst       = 8.8.8.8
           \options   \
###[ ICMP in ICMP ]### 
              type      = echo-request
              code      = 0
              chksum    = 0xf7ff
              id        = 0x0
              seq       = 0x0

