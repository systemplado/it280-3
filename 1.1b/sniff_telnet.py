from scapy.all import *


def print_pkt(pkt):
  pkt.show()


pkt = sniff(filter='src net 10.0.4.80 and dst port 23', prn=print_pkt)
