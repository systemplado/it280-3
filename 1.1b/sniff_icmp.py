from scapy.all import *


def print_pkt(pkt):
  print('- - - - - - - - - - - - - - - - - -')
  pkt.show()


pkt = sniff(filter='icmp', prn=print_pkt)
